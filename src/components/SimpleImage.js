// components/Container.js

import React from 'react';
import image1 from '../assets/img/scenery/image1.jpg'
import image4 from '../assets/img/scenery/image4.jpg'
import image6 from '../assets/img/scenery/image6.jpg'

function Container() {
  return (
    <section class="clean-block slider dark">
            <div class="container">
                <div class="block-heading">
                    <h2 class="text-dark">ตัวอย่างรูปภาพ</h2>
                </div>
                <div class="carousel slide" data-bs-ride="carousel" id="carousel-1">
                    <div class="carousel-inner">
                        <div class="carousel-item active"><img class="w-100 d-block" src={image1} alt="Slide Image"></img></div>
                        <div class="carousel-item"><img class="w-100 d-block" src={image4} alt="Slide Image"></img></div>
                        <div class="carousel-item"><img class="w-100 d-block" src={image6} alt="Slide Image"></img></div>
                    </div>
                    <div><a class="carousel-control-prev" href="#carousel-1" role="button" data-bs-slide="prev"><span class="carousel-control-prev-icon"></span><span class="visually-hidden">Previous</span></a><a class="carousel-control-next" href="#carousel-1" role="button" data-bs-slide="next"><span class="carousel-control-next-icon"></span><span class="visually-hidden">Next</span></a></div>
                    <div class="carousel-indicators"><button type="button" data-bs-target="#carousel-1" data-bs-slide-to="0" class="active"></button> <button type="button" data-bs-target="#carousel-1" data-bs-slide-to="1"></button> <button type="button" data-bs-target="#carousel-1" data-bs-slide-to="2"></button></div>
                </div>
            </div>
        </section>
  );
}

export default Container;
