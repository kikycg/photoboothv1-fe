// components/Container.js

import React from 'react';
import img_qrcode from '../assets/img/QRCODE.png'

function Container() {
  return (
    <section class="clean-block clean-info dark">
            <div class="container">
                <div class="block-heading">
                    <h2 class="text-dark">สแกนรับภาพด่วน</h2>
                    <p>สแกน QR Code รับรูปด่วนได้ทันที</p>
                </div>
                <div class="row align-items-center">
                    <div class="col-md-6"><img class="img-thumbnail" src={img_qrcode}></img></div>
                    <div class="col-md-6">
                        <h3>Scan QR Code เพื่อรับภาพ</h3>
                        <div class="getting-started-info">
                            <p>Scan QR Code นี้เพื่อดาวน์โหลดรูปผ่าน Google Photo หรือเลื่อนดูภาพด้านล่าง</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
  );
}

export default Container;
