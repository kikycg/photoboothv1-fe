import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import iconscan from "../iconscan.png";

function Container() {
  const host_image =  process.env.REACT_APP_API_URL+"/"
  const host_scan = process.env.REACT_APP_API_AI_URL

  const [images, setImages] = useState([]);
  const fileInputRef = useRef(null);

  const handleImageChange =async (event) => {
    console.log(event.target.files[0]);


    const formData = new FormData();
    formData.append('image_upload', event.target.files[0]);

    axios.post(host_scan+'/faces_recognition/', formData)
      .then(response => {
        console.log('Upload successful:', response.data);
        // Assuming the response contains the image URL after upload
        console.log(response.data);
        setImages(response.data.image);
      })
      .catch(error => {
        console.error('Error uploading image:', error);
        // Handle error, e.g., show an error message
      });

  };

  const handleDivClick = () => {
    fileInputRef.current.click();
    // handleUpload()
    // handleImageUpload()
  };

  

  return (
    <section className="clean-block features">
      <div className="container">
        <div className="block-heading">
          <section className="photo-gallery py-4 py-xl-5">
            <div className="container">
              <div
                onClick={handleDivClick}
                class="container text-white bg-primary border rounded border-0 p-4 p-lg-5 py-4 py-xl-4"
              >
                <div class="row mb-5">
                  <div class="col-md-8 col-xl-6 text-center mx-auto">
                    <img class="img-fluid" src={iconscan} />
                    <h2 class="text-white"></h2>
                    <h2 class="text-white">แตะที่นี้</h2>
                    <p class="w-lg-50">ถ่ายภาพใบหน้าของคุณเพื่อค้นหารูปภาพ</p>
                    <input
                      type="file"
                      accept="image/*"
                      onChange={handleImageChange}
                      style={{ display: "none" }}
                      ref={fileInputRef}
                    />
                  </div>
                </div>
              </div>
              <div className="row mb-5">
                <div className="col-md-8 col-xl-6 text-center mx-auto">
                  <h2></h2>
                </div>
              </div>
              <div className="row gx-2 gy-2 row-cols-2 row-cols-md-2 row-cols-xl-5 photos">
                
              {images && images.map((image, index) => (
                  <div className="col item" key={index}>
                    <img
                      className="img-fluid"
                      src={host_image+image.file}
                      alt={`API Image ${index}`}
                      style={{ cursor: "pointer" }}
                      // onClick={() => openModal(`${image.file}`, "Snow")}
                    />
                  </div>
                ))}



              </div>
            </div>
          </section>
        </div>
      </div>

      <div>
        {/* The Modal
      {modalVisible && (
        <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          position: 'fixed',
          zIndex: 1,
          left: 0,
          top: 0,
          width: '100%',
          height: '100%',
          overflow: 'auto',
          backgroundColor: 'rgba(0,0,0,0.9)',
        }}
        >
          <span
            className="close"
            style={{
              position: 'absolute',
              top: '10%',
              right: '10%',
              color: '#f1f1f1',
              fontSize: '40px',
              fontWeight: 'bold',
              transition: '0.3s',
              cursor: 'pointer',
            }}
            onClick={closeModal}
          >
            &times;
          </span>
          <img
            className="modal-content"
            src={selectedImage}
            alt={caption}
            style={{ margin: 'auto', display: 'block', width: '70%', maxWidth: '70%' }}
          />
          <button
            // onClick={handleDownload}
            style={{
              position: 'absolute',
              bottom: '10%',
              left: '50%',
              transform: 'translateX(-50%)',
              padding: '10px',
              background: '#4CAF50',
              color: 'white',
              border: 'none',
              borderRadius: '5px',
              cursor: 'pointer',
            }}
          >
            Download
          </button>
        </div>
      )} */}
      </div>
    </section>
  );
}

export default Container;
