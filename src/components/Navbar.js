
// components/Navbar.js

import React from 'react';
import { Link } from 'react-router-dom';

function Navbar() {
  return (
    <nav class="navbar navbar-expand-lg fixed-top bg-body clean-navbar navbar-light">
        <div class="container"><Link class="navbar-brand logo" to="/">KAPAOKY</Link><button data-bs-toggle="collapse" class="navbar-toggler" data-bs-target="#navcol-1"><span class="visually-hidden">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item"><Link class="nav-link" to="/">Home</Link></li>
                    {/* <li class="nav-item"><a class="nav-link" href="pricing.html">Pricing</a></li>
                    <li class="nav-item"><a class="nav-link active" href="about-us.html">About Us</a></li>
                    <li class="nav-item"><a class="nav-link" href="contact-us.html">Contact Us</a></li> */}
                </ul>
            </div>
        </div>
    </nav>
  );
}

export default Navbar;
