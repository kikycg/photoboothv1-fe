// components/Container.js

import React, { useState, useEffect } from "react";
import axios from "axios";
import InfiniteScroll from "react-infinite-scroll-component";
import { Link } from "react-router-dom";

function Container() {


  const host = process.env.REACT_APP_API_URL


  const [images, setImages] = useState([]);
  const [posts, setPosts] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [selectedImage, setSelectedImage] = useState("");
  const [caption, setCaption] = useState("");
  const [page, setPage] = useState(1);

  const fetchData = async () => {
    try {
      const response = await axios.get(
        `${host}/photo?page=${page}&limit=10`
      ); // เปลี่ยน URL ให้ตรงกับ API ที่คุณใช้

      setPosts((prevPosts) => [...prevPosts, ...response.data.image]);
      setPage((prevPage) => prevPage + 1);
      console.log(response.data.image);
    } catch (error) {
      console.error("Error fetching posts:", error);
    }
  };

  const openModal = (src, alt) => {
    console.log(src);
    setSelectedImage(src);
    setCaption(alt);
    setModalVisible(true);
  };

  const closeModal = () => {
    setModalVisible(false);
  };

  const handleDownload =async () => {
    const imageUrl = selectedImage;

    try {
      // Fetch the image data
      const response = await fetch(imageUrl);
      const blob = await response.blob();

      // Create a link element
      const link = document.createElement('a');

      // Create an object URL for the blob
      const objectUrl = URL.createObjectURL(blob);

      // Set the href attribute to the object URL
      link.href = objectUrl;

      // Set the download attribute with the desired file name (you can include the file extension)
      link.download = selectedImage.split("/")[selectedImage.split("/").length-1]; // Replace with your desired file name

      // Append the link to the document
      document.body.appendChild(link);

      // Trigger a click on the link to start the download
      link.click();

      // Remove the link and revoke the object URL
      document.body.removeChild(link);
      URL.revokeObjectURL(objectUrl);
    } catch (error) {
      console.error('Error downloading image:', error);
    }

  };

  const infiniteScrollStyle = {
    display: "flex",
    flexDirection: "column", // Adjust the styles as needed
    alignItems: "center",
    padding: "20px",
  };

  return (
    <section className="clean-block features">
      <div className="container">
        <div className="block-heading">
          <section className="photo-gallery py-4 py-xl-5">
            <div className="container">
              <div className="row mb-5">
                <div className="col-md-8 col-xl-6 text-center mx-auto">
                  <h2>รูปภาพทั้งหมดในงาน</h2>
                </div>
              </div>
              <InfiniteScroll
                dataLength={posts.length}
                next={fetchData}
                hasMore={true}
                className="row gx-2 gy-2 row-cols-2 row-cols-md-2 row-cols-xl-5 photos"
              >
                {posts.map((image, index) => (
                  <div className="col item" key={index}>
                    <img
                      className="img-fluid"
                      src={image.file}
                      alt={`API Image ${index}`}
                      style={{ cursor: "pointer" }}
                      onClick={() => openModal(`${image.file}`, "Snow")}
                    />
                  </div>
                ))}

                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    position: "fixed",
                    zIndex: 1,
                    left: "35%",
                    top: "88%",
                    width: "35%",
                    height: "8%",
                    backgroundColor: "rgba(255,255,255,1)",
                    background: "#FFFFF",
                    // color: "white",
                    border: "none",
                    borderRadius: "5px",
                  }}
                >
                  <Link to="/search">ค้นหารูปภาพด้วย AI</Link>
                </div>
              </InfiniteScroll>

              {/* </div> */}
            </div>
          </section>
        </div>
      </div>

      <div>
        {/* The Modal */}
        {modalVisible && (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              position: "fixed",
              zIndex: 1,
              left: 0,
              top: 0,
              width: "100%",
              height: "100%",
              overflow: "auto",
              backgroundColor: "rgba(0,0,0,0.9)",
            }}
          >
            <span
              className="close"
              style={{
                position: "absolute",
                top: "10%",
                right: "10%",
                color: "#f1f1f1",
                fontSize: "40px",
                fontWeight: "bold",
                transition: "0.3s",
                cursor: "pointer",
              }}
              onClick={closeModal}
            >
              &times;
            </span>
            <img
              className="modal-content"
              src={selectedImage}
              alt={caption}
              style={{
                margin: "auto",
                display: "block",
                width: "70%",
                maxWidth: "70%",
              }}
            />
            <button
              onClick={handleDownload}
              style={{
                position: "absolute",
                bottom: "10%",
                left: "50%",
                transform: "translateX(-50%)",
                padding: "10px",
                background: "#4CAF50",
                color: "white",
                border: "none",
                borderRadius: "5px",
                cursor: "pointer",
              }}
            >
              Download
            </button>
          </div>
        )}
      </div>
    </section>
  );
}

export default Container;
