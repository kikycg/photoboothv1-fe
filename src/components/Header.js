// components/Header.js

import React from 'react';

function Header() {
  return (
    <header>
      <meta charset="utf-8"></meta>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no"></meta>
      <h1>My Website</h1>
    </header>
  );
}

export default Header;
