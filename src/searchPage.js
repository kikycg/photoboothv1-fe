import React from 'react';

import Header from './components/Header';
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import Search from './components/Search';


const App = () => {
  return (
      <div>
        <Header />
        <Navbar />
        <Search/>
        <Footer />
      </div>
  );
};


export default App;