import React from 'react';
import Header from './components/Header';
import Navbar from './components/Navbar';
import Container from './components/Container';
import SimpleImage from './components/SimpleImage';
import AllImage from './components/AllImage';
import Footer from './components/Footer';

import './assets/bootstrap/css/bootstrap.min.css';

import './css/styles.min.css';




const App = () => {
  return (
      <div>
        <Header />
        <Navbar />
        <Container />
        {/* <SimpleImage /> */}
        <AllImage />
        <Footer />
      </div>
  );
};


export default App;